#!/bin/sh
# Script to remove all phisical network cards after boot
# unless the "nonetdisable" kernel boot parameter is used
# Use the "-f" option to remove the interfaces even if
# the "nonetdisable" parameter is provided

PATH=/bin:/usr/bin:/sbin:/usr/sbin ; export PATH

if [ "$1" != "-f" ] ; then
    for arg in `cat /proc/cmdline` ; do
        [ "$arg" = nonetdisable ] && exit 0
    done
fi

for dev in /sys/class/net/* ; do 
    if udevadm info --path=$dev 2>/dev/null | \
        grep ID_NET_NAME_PATH > /dev/null 2>&1 ; then
        path=`udevadm info --path=$dev | grep ^P: | sed 's/^P: //'`
        path=/sys$path
        for i in 0 1 2 3 4 ; do
            [ -d $path ] || break
            if [ -f "${path}/remove" ] ; then
                echo 1 > "${path}/remove"
                break
            fi
            path="${path}/.."
        done
    fi
done

