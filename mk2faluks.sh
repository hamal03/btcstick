#!/bin/bash

export PATH="/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin"

#This script will create a file on an automounted removable device and
#create a Luks encrypted filesystem on that file. The encryption password
#is generated from user input and the serial number of the device.
#The system expects an overlay root filesystem and no network present. These
#can be disabled on the command line. The system will create a vfat
#filesystem by default. This can be overridden on the command line.
#
#The user running this must be able to run some commands as root via sudo
#i.e.: cryptsetup, losetup, mkfs.*, mount, umount

fatal() {
    zenity --error --text "$@" --width 400 2>/dev/null
    exit 1
}

inarr() {
    local arrelem
    local item="$1"
    shift
    for arrelem ; do
        [ "$arrelem" = "$item" ] && return 0
    done
    return 1
}

CLARGS=""
while [ $# -gt 0 ] ; do
    if [ "$1" = "-h" -o "$1" = "--help" ] ; then
        echo "This script creates a file on an automounted  USB disk and creates"
        echo "   a Luks encryped filesystem on it. The password that is used for"
        echo "   the encryption is generated from a user password input and from"
        echo "   the serial number of the USB disk it resides on."
        echo ""
        echo "Usage:"
        echo "   $0 [-r|--nocheckoverlayroot] [-n|--nocheckoffline] [-f|--force] "
        echo "       [-t|--fs FILESYSTEM] [-s|--size SIZE] /path/to/new/file"
        echo "   $0 [-h|--help]"
        echo ""
        echo "-h: print this help text and exit"
        echo "-r: don't check for an overlayfs on / (i.e. allow persistent"
        echo "    writes on the root filesystem)"
        echo "-n: don't check for absence of a network connection (allow online state)"
        echo "-f: Allow both online state and persistent writes"
        echo "-t FILESYSTEM: create this filesystem type (default is exfat)"
        echo "    Allowed filesystem type: exfat, vfat, ext2, ext4"
        echo "-s SIZE: specify the size of the file. The size must be in a format that"
        echo "    the 'dd' command understands"
        echo "The full path to a file to be created must be provided as the last parameter. "
        echo "    That path must be on an automounted USB device (in /media/username)."
        exit 0
    elif [ "$1" = "-r" -o "$1" = "--nocheckoverlayroot" ]; then
        CLARGS=" -r$CLARGS"
        shift
    elif [ "$1" = "-n" -o "$1" = "--nocheckoffline" ]; then
        CLARGS=" -n$CLARGS"
        shift
    elif [ "$1" = "-f" -o "$1" = "--force" ]; then
        CLARGS=" -r -n$CLARGS"
        shift
    elif [ "$1" = "-t" -o "$1" = "--fs" ]; then
        FS="$2" # vfat, exfat, ext2, ext4
        shift ; shift
    elif [ "$1" = "-s" -o "$1" = "--size" ]; then
        SIZE="$2"
        shift; shift
    elif [[ "$1" =~ ^- ]] ; then
        fatal "Unknown option: $1"
    else
        luksfile="$1"
        break
    fi
done
[ -z "$luksfile" ] && fatal "No file specified to create"
[ -e "$luksfile" ] && fatal "$luksfile already exists"
FS=${FS:-exfat} ; FS=${FS,,}
inarr "$FS" exfat vfat ext2 ext4 || \
    fatal "Unknown filesystem specified: $FS"

CURUSR=$(id -un)
[[ "$luksfile" =~ ^/media/${CURUSR}/ ]] || \
    fatal "$(dirname $luksfile) is not on an automounted filesystem"
[ -w "$(dirname $luksfile)" ] || \
    fatal "$(dirname $luksfile) is not a writable directory"
dd if=/dev/zero bs=${SIZE:-100M} count=0 seek=1 >/dev/null 2>&1 || \
    fatal "Unknown file size: $SIZE"

usbpw=$(getusbpasswd.sh $CLARGS "$(dirname $luksfile)")
[ $? -ne 0 ] && exit 1
usbpw="${usbpw## : }"
[ -z "$usbpw" ] && \
    fatal "Could not get a 2FA password for $(dirname $luksfile)"

# Create file with pseudorandom data
fifo="/run/user/$(id -u)/2fa-progress" ; mkfifo "$fifo"
(cat "$fifo" | zenity --progress --pulsate --auto-close --text \
    "Filling a file with pseudorandom data\nPlease be patient" 2>/dev/null) &
dd if=/dev/urandom bs=${SIZE:-100M} count=1 2>/dev/null | \
    dd of="$luksfile" bs=16M oflag=sync 2>/dev/null
[ $? -ne 0 ] && {
    echo finish > "$fifo"
    rm "$fifo"
    rm -f "$luksfile"
    fatal "Could not create $luksfile"
}
echo finish > "$fifo"
rm "$fifo"

ownpw=$(passphrase-zxcvbn --confirm --title "$luksfile" --text \
    "Input user Luks password") 2>/dev/null
if [ -z "$ownpw" ] ; then
    rm $luksfile 2>/dev/null
    fail "No password has been provided"
fi
finalpw=$(openssl dgst -sha256 -binary <<< "${usbpw}${ownpw}" | openssl enc -a)
finalpw=${finalpw:0:24}

sudo cryptsetup luksFormat --hash sha256 "$luksfile" <<< "$finalpw" 2>/dev/null
if [ $? -ne 0 ] ; then
    rm $luksfile 2>/dev/null
    fail "Failed creating a Luks header on $luksfile"
fi
mapname=$(basename $(mktemp -u /dev/mapper/$(basename $luksfile).XXXX))
sudo cryptsetup luksOpen "$luksfile" $mapname <<< "$finalpw"
[ $? -ne 0 ] && fatal "This cryptsetup should not fail"
if [[ "$FS" =~ ^ext.$ ]] ; then
    sudo mkfs.$FS -E root_owner=$(id -u):$(id -g) -L "$(basename $luksfile)" \
        /dev/mapper/${mapname} >/dev/null 2>&1
elif [ "$FS" = "msdos" ] ; then
    sudo mkfs.msdos -n "$(basename $luksfile)"  /dev/mapper/${mapname} >/dev/null 2>&1
else
    sudo mkfs.exfat -n "$(basename $luksfile)"  /dev/mapper/${mapname} >/dev/null 2>&1
fi
if [ $? -ne 0 ] ; then
    sudo cryptsetup luksClose $mapname
    rm "$luksfile"
    fatal "Could not format $luksfile"
fi

sudo cryptsetup luksClose $mapname
zenity --info --text "Luks encrypted filesystem with 2FA created on\n$luksfile" \
    --width 500 2>/dev/null
