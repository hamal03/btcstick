BTCStick scripts
================

This repository contains a couple of scripts that are used in the
[BTCStick](https://techblog.hamal.nl/btcstick.html) USB pen drive to
use various bitcoin-related tools from a guaranteed off-line environment.
The project is inspired by the [Bitkey project](https://bitkey.io) and
one of the scripts is copied from that project.

## Pseudo Two Factor Luks
These scripts create and mount files that contain Luks-encrypted file
systems. The passwords for such encryption is computed from two inputs
i.e. a user provided passphrase and the serial number of the USB pendrive 
that the file resides on.

Since these scripts are written for the BTCStick project, the scripts
that create or mount an encrypted filesystem check if the environment
has no network interfaces available and if the root filesystem is mounted
with a ramfs overlay. The scripts will terminate with an error by default
if any of these are not true.

A pre-built image is available from Google Drive via https://ham.al/btcstick
The zip file that is retrieved contains a PGP encrypted file with which
the consistency of the image can be validated. The key used for signing
the file is d1f8ffb9f7a0f7a0 and it can be retrieved from a key server e.g.
http://keyserver.ubuntu.com/pks/lookup?op=get&search=0xd1f8ffb9f7a0f7a0

