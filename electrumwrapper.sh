#!/bin/bash

export PATH="/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin"

# This will look for a file named electrum.vol in the /btcstick directory on
# an automounted USB stick, mount it and start electrum with that mount as a
# directory. The file may also be provided on the command line.

fatal() {
    zenity --error --text "$@" --width 400 2>/dev/null
    exit 1
}

CURUSR=$(id -un)
findelcs() {
    retval=1
    for dir in /media/${CURUSR}/* ; do
        [ -d "$dir" ] || continue
        [ -w "$dir" ] && retval=0
        [ -f "${dir}/btcstick/electrum.vol" -a -w "${dir}/btcstick/electrum.vol" ]  && \
            echo "${dir}/btcstick/electrum.vol"
    done
    return $retval
}

CLARGS=""
while [ $# -gt 0 ] ; do
    if [ "$1" = "-h" -o "$1" = "--help" ] ; then
        echo "This program uses a provided file on an automounted USB drive or "
        echo "   searches for electrum.vol on such drives, uses Luks and mounts"
        echo "   it on a temprary directory to start the Electrum wallet from."
        echo "   The script expects an overlay root filesystem and no network"
        echo "   present (i.e., offline read-only mode) and aborts if these are"
        echo "   absent. This can be disabled with CL options."
        echo "   The script uses the USB serial number as part of the password."
        echo "   This too can be disabled on the command line."
        echo ""
        echo "Usage:"
        echo "   $0 [-r|--nocheckoverlayroot] [-n|--nocheckoffline] [-U|--nousbpassword]"
        echo "       [-f|--force] [/file/or/dir]"
        echo "   $0 [-h|--help]"
        echo ""
        echo "-h: print this help text and exit"
        echo "-r: don't check for an overlayfs on / (i.e. allow persistent"
        echo "    writes on the filesystem)"
        echo "-n: don't check for absence of a network connection (allow online state)"
        echo "-U: don't query the USB serial number to seed the password (disable 2FA,"
        echo "    ignored when creating a new file)"
        echo "-f: Allow both online state and persistent writes"
        echo "The full path to a Luks encrypted file may be provided as the last "
        echo "    parameter, that path is used to determine which USB stick to use"
        exit 0
    elif [ "$1" = "-r" -o "$1" = "--nocheckoverlayroot" ]; then
        CLARGS=" -r$CLARGS"
        shift
    elif [ "$1" = "-n" -o "$1" = "--nocheckoffline" ]; then
        CLARGS=" -n$CLARGS"
        shift
    elif [ "$1" = "-f" -o "$1" = "--force" ]; then
        CLARGS=" -r -n$CLARGS"
        shift
    elif [ "$1" = "-U" -o "$1" = "--nousbpassword" ]; then
        NO2FA=1
        shift
    elif [[ "$1" =~ ^- ]] ; then
        fatal "Unknown option: $1"
    else
        elcfile="$1"
        break
    fi
done

if [ -n "$elcfile" ] ; then
    [ -f $elcfile -a -w $elcfile ] || fatal "Provided file doesn't exist of is not writable"
else
    elcfile=$(findelcs)
    retval=$?
    if [ -z "$elcfile" ]; then
        [ "$retval" -ne 0 ] && fatal "No encrypted Electrum file found\n(and no way to create one)"
        zenity --question --text "No encrypted Electrum file found\nCreate one?" \
            --width 400 2>/dev/null
        [ $? -ne 0 ] && {
            zenity --warning --text "Aborting on user request" --width 250 2>/dev/null
            exit 1
        }
        elcdir=$(
            for dir in /media/${CURUSR}/* ; do
                [ -d "$dir" -a -w "$dir" ] && echo "$dir"
            done
        )
        [ -z "$elcdir" ] && fatal "No directory to create Electrum file on"
        mapfile -t fcnt <<< "$elcdir"
        if [ ${#fcnt[@]} -gt 1 ] ; then
            zarr=()
            for item in "${fcnt[@]}" ; do
                zarr+=('FALSE')
                zarr+=("$item")
            done
            elcdir=$(zenity --title "Pick a device to use" --list --radiolist \
                --column "" --column "Directory" "${zarr[@]}") 2>/dev/null
            [ -n "$elcdir" ] || fatal "No direcory has been chosen."
        fi
        mkdir -p ${elcdir}/btcstick
        [ $? -ne 0 ] && fatal "Could not create direcory ${elcdir}/btcdir"
        mk2faluks.sh $CLARGS "${elcdir}/btcstick/electrum.vol"
        [ $? -ne 0 ] && exit 1
        elcfile="${elcdir}/btcstick/electrum.vol"
    else
        mapfile -t fcnt <<< "$elcfile"
        if [ ${#fcnt[@]} -gt 1 ] ; then
            zarr=()
            for item in "${fcnt[@]}" ; do
                zarr+=('FALSE')
                zarr+=("$item")
            done
            elcfile=$(zenity --title "Pick a file to use" --list --radiolist \
                --column "" --column "File" "${zarr[@]}") 2>/dev/null
            [ -n "$elcfile" ] || fatal "No file has been chosen."
        fi
    fi
fi

if [ -z "$NO2FA" -o -n "$elcdir" ] ; then
    usbpw=$(getusbpasswd.sh $CLARGS "$elcfile")
    [ $? -ne 0 ] && exit 1
    [ -z "$usbpw" ] && fatal "We could not get a USB serial number"
    usbpw="${usbpw## : }"
else
    usbpw=""
fi

ownpw=$(zenity --password --title "Input user password seed" 2>/dev/null)
[ -z "$ownpw" ] && fatal "No password was entered"

if [ -n "$usbpw" ] ; then
    # Convert both seeds to the password to use
    finalpw=$(openssl dgst -sha256 -binary <<< "${usbpw}${ownpw}" | openssl enc -a)
    finalpw=${finalpw:0:24}
else
    finalpw=$ownpw
fi

# Mount it
mappername=$(basename $(mktemp -u /dev/mapper/electrum.XXXX))
sudo cryptsetup luksOpen "$elcfile" $mappername <<< "$finalpw"
[ $? -ne 0 ] && \
    fatal "Could not open encrypted file \"$elcfile\" (wrong password?)"
mpoint=$(mktemp -d)
file - < /dev/mapper/$mappername | grep ' ext. filesystem'  || \
    FSOPT="-o uid=$(id -u)"
sudo mount $FSOPT /dev/mapper/${mappername} ${mpoint}
if [ $? -ne 0 ] ; then
    rmdir $mpoint
    sudo cryptsetup luksClose $mappername
    fatal "Could not mount encrypted fs $elcfile"
fi

electrum -D $mpoint

# tear it down
sudo umount $mpoint
if [ $? -ne 0 ] ; then
    fuser -kmu $mpoint
    sudo umount $mpoint
fi
rmdir $mpoint
sudo cryptsetup luksClose $mappername

