#!/bin/bash

export PATH="/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin"

ERRMSG=""

for dev in /sys/class/net/* ; do
    if udevadm info --path=$dev 2>/dev/null | \
        grep ID_NET_NAME_PATH > /dev/null 2>&1 ; then
        ERRMSG="WARNING\nNetwork device detected"
        break
    fi
done

if ! df / | grep ^overlayroot > /dev/null 2>&1 ; then
    if [ -n "$ERRMSG"  ] ; then
        ERRMSG="${ERRMSG}\nAND\n"
    else
        ERRMSG="WARNING\n"
    fi
    ERRMSG="${ERRMSG}Root filesystem persistently mounted"
fi

if [ -n "$ERRMSG" ] ; then
    zenity --warning --text "$ERRMSG" --width 400 2>/dev/null &
else
    OKMSG="You are safely running off-line with\n"
    OKMSG="${OKMSG}a non-persistent root filesystem"
    gsettings set org.mate.background picture-filename ${HOME}/Pictures/Coins.jpg
    zenity --info --text "$OKMSG" --width 400 2>/dev/null &
fi
