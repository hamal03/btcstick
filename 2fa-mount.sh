#!/bin/bash

export PATH="/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin"

# This script will mount a Luks encrypted filesystem from a provided
# file. The password is by default generated from the user provided
# passphrase AND the serial number of the USB device it resides on.
# The script also checks for an overlayroot filesystem and the absence
# of a network device

fatal() {
    zenity --error --text "$@" --width 400 2>/dev/null
    exit 1
}

CLARGS=""
while [ $# -gt 0 ] ; do
    if [ "$1" = "-h" -o "$1" = "--help" ] ; then
        echo "Mount or unmount a Luks encrypted file using a usb-serial based pseudo-2FA"
        echo "   password. "
        echo ""
        echo "Usage:"
        echo "   $(basename $0) [-h|--help]"
        echo "   2fa-mount.sh [-r|--nocheckoverlayroot] [-n|--nocheckoffline] [-f|--force] "
        echo "       [-1|--1fa] /path/to/luksfile [/path/to/mountpoint]"
        echo "   2fa-umount.sh /path/tp/mountpoint"
        echo ""
        echo "-h: print this help text and exit"
        echo "-r: don't check for an overlayfs on / (i.e. allow persistent"
        echo "    writes on the filesystem)"
        echo "-n: don't check for absence of a network connection (allow online state)"
        echo "-f: Allow both online state and persistent writes"
        echo "-1: don't use 2FA, only the user password"
        echo "The encrypted file to mount is mandatory, the moint point to mount it on"
        echo "    is optional. If omitted, the file is mounted on a temporary directory."
        echo "When unmounting, the mount point is mandatory and no flags are expected."
        echo ""
        exit 0
    elif [ "$1" = "-r" -o "$1" = "--nocheckoverlayroot" ]; then
        CLARGS=" -r$CLARGS"
        shift
    elif [ "$1" = "-n" -o "$1" = "--nocheckoffline" ]; then
        CLARGS=" -n$CLARGS"
        shift
    elif [ "$1" = "-f" -o "$1" = "--force" ]; then
        CLARGS=" -r -n$CLARGS"
        shift
    elif [ "$1" = "-1" -o "$1" = "--1fa" ]; then
        ONEFA=1
        shift
    elif [[ "$1" =~ ^- ]] ; then
        fatal "Unknown option: $1"
    else
        break
    fi
done

if [ $(basename $0) = "2fa-mount.sh" ] ; then
    [ $# -eq 0 ] && fatal "No file specified to mount"
    luksfile="$1"
    [ -f "$luksfile" -a -w "$luksfile" ] || \
        fatal "$luksfile does not exists or is not a writable file"
    if [ -n "$2" ] ; then
        mpoint="$2"
        [ -d "$mpoint" -o -x "$mpoint" ] || \
            fatal "$mpoint is not usable as a mount point"
    else
        mpoint=$(mktemp -d -p ${HOME} usbmount.XXXXX)
    fi

    if [ -z "$ONEFA" ] ; then
        usbpw=$(getusbpasswd.sh $CLARGS "$luksfile")
        [ $? -ne 0 ] && exit 1
        [ -z "$usbpw" ] && fatal "We could not get a USB serial number"
        usbpw="${usbpw## : }"

        ownpw=$(zenity --password --title "Input user password seed" 2>/dev/null)
        [ -z "$ownpw" ] && fatal "No password was entered"

        # Convert both seeds to the password to use
        finalpw=$(openssl dgst -sha256 -binary <<< "${usbpw}${ownpw}" | openssl enc -a)
        finalpw=${finalpw:0:24}
    else
        finalpw=$(zenity --password --title "Input user password seed" 2>/dev/null)
        [ -z "$finalpw" ] && fatal "No password was entered"
    fi

    mappername=$(basename $(mktemp -u /dev/mapper/luksfile.XXXX))
    sudo cryptsetup luksOpen "$luksfile" "$mappername" <<< "$finalpw"
    [ $? -ne 0 ] && \
        fatal "Could not open encrypted file \"$luksfile\" (wrong password?)"
    sudo dd if=/dev/mapper/$mappername bs=1b count=1 2>/dev/null | file - | \
        grep -q ' ext. filesystem' || FSOPT="-o uid=$(id -u)"
    sudo mount $FSOPT /dev/mapper/${mappername} ${mpoint}
    if [ $? -ne 0 ] ; then
        [[ $mpoint =~ ^${HOME}/usbmount\..{5}$ ]] && rmdir $mpoint
        sudo cryptsetup luksClose $mappername
        fatal "Could not mount encrypted fs $elcfile"
    fi
    zenity --info --text "Encrypted filesystem in $luksfile mounted on $mpoint" \
        --width 600 2>/dev/null
else
    [ $# -eq 0 ] && fatal "Directory to unmount not specified"
    mpoint="$1"
    [[ $mpoint =~ ^/ ]] || mpoint=$(pwd)/$mpoint
    mapper=$(mount | grep " on ${mpoint} type" 2>/dev/null)
    mapper=${mapper%% *} ; mapper=${mapper#/dev/mapper/}
    sudo cryptsetup status "$mapper" >/dev/null 2>&1 || \
        fatal "The mountpoint $mpoint is not from an encrypted device"
    # kill remaining processes on the mount point
    sudo fuser -km "$mpoint"
    sudo umount "$mpoint" || fatal "Could not unmount $mpoint"
    [[ $mpoint =~ ^${HOME}/usbmount\..{5}$ ]] && rmdir $mpoint
    sudo cryptsetup luksClose "$mapper" || \
        fatal "Stopping device mapping failed"
    zenity --info --text "Unmounted $mpoint" --width 300 2>/dev/null
fi

