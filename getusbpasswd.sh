#!/bin/bash -e

export PATH="/bin:/usr/bin"

# 2019-11-05 - RSW
# This script will look for "automounted" removable devices (i.e., 
# devices mounted on /media/userid/somedirectory) and return  the 
# directory and serial number of that device. If more than one
# mount point is present and the user has not specified the file
# or directory to use, the user is presented with a radiolist
# to choose the directory.
#
# This script also checks if there is an active network and a
# non-overlay root filesystem and by default refuse to run un that 
# case (because we will want to use this on a strictly off-line
# system). This can be overridden with command line options.

fatal() {
    zenity --error --text "$@" --width 400 2>/dev/null
    exit 1
}

checkoffline() {
    for dev in /sys/class/net/* ; do
        if udevadm info --path=$dev 2>/dev/null | \
            grep ID_NET_NAME_PATH > /dev/null 2>&1 ; then
            fatal "Network device detected, exiting"
        fi
    done
}

checkoverlayroot() {
    if ! df / | grep ^overlayroot > /dev/null 2>&1 ; then
        fatal "OverlayRoot not detected, exiting"
    fi
}

getdirectory() {
    CURUSR=$(id -un)
    if [ -n "$1" ] ; then
        arg="$1"
        [ -e "$1" ] || fatal "File or firectory not present"
        [[ "$arg" =~ ^/media/${CURUSR}/.+ ]] || fatal "Provided directory not automounted"
        if [ -f "$arg" -a -w "$arg" ] ; then
            echo $(dirname "$arg")
        elif [ -d "$arg" -a -w "$arg" ] ; then
            echo "$arg"
        fi
    else
        for dir in /media/${CURUSR}/* ; do
            [ -d "$dir" ] || continue
            [ -w "$dir" ]  && echo "$dir"
        done
    fi
}

getdiskserial() {
    local dirname="$1"
    local fs=$(df "$dirname"|grep -o '^/dev/[^ ]*')
    [ -z "$fs" ] && return 1
    local ud=$(udevadm info --name="$fs")
    mapfile -t udvars <<< "$ud"
    [ "${#udvars[@]}" -eq 0 ] && return 1
    for ev in i"${udvars[@]}" ; do
        if [[ "$ev" =~ ID_SERIAL_SHORT ]] ; then
            echo ${ev#*ID_SERIAL_SHORT=}
            break
        fi
    done
}

while [ $# -gt 0 ] ; do
    if [ "$1" = "-h" -o "$1" = "--help" ] ; then
        echo "This program fetches the serial number of an automounted USB stick and echoes"
        echo "    it to stdout. The purpose is to use it one factor of a simple 2FA password"
        echo ""
        echo "Usage:"
        echo "   $0 [-r|--nocheckoverlayroot] [-n|--nocheckoffline] [-f|--force] [/file/or/dir]"
        echo "   $0 [-h|--help]"
        echo ""
        echo "-h: print this help text and exit"
        echo "-r: don't check for an overlayfs on / (i.e. allow persistent"
        echo "    writes on the filesystem)"
        echo "-n: don't check for absence of a network connection (allow online state)"
        echo "-f: Allow both online state and persistent writes"
        echo "The full path to a file or directory may be provided as the last parameter, "
        echo "    that path is used to determine which USB stick to use"
        exit 0
    elif [ "$1" = "-r" -o "$1" = "--nocheckoverlayroot" ]; then
        NCOR=1
        shift
    elif [ "$1" = "-n" -o "$1" = "--nocheckoffline" ]; then
        NCNW=1
        shift
    elif [ "$1" = "-f" -o "$1" = "--force" ]; then
        NCOR=1
        NCNW=1
        shift
    elif [[ "$1" =~ ^- ]] ; then
        fatal "Unknown option: $1"
    else
        fileordir="$1"
        break
    fi
done

[ -n "$NCOR" ] || checkoverlayroot
[ -n "$NCNW" ] || checkoffline
usbdir=$(getdirectory $fileordir)
[ -n "$usbdir" ] || fatal "No automounted directory present"
mapfile -t dirarr <<< "$usbdir"
if [ ${#dirarr[@]} -gt 1 ] ; then
    zarr=()
    for item in "${dirarr[@]}" ; do
        zarr+=('FALSE')
        zarr+=("$item")
    done
    usbdir=$(zenity --title "Pick a USB durectory" --list --radiolist \
        --column "" --column "Directory" "${zarr[@]}") 2>/dev/null
fi
[ -z "$usbdir" ] && fatal "No choice has been made"
dsksrl=$(getdiskserial "$usbdir")
[ -z "$dsksrl" ] && fatal "Could not get a serial number from $usbdir"
echo "${usbdir} : $dsksrl"

