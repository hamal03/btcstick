#!/bin/bash

PYVENV=$(basename $0)
VENVDIR="${VENVDIR:-/usr/local/lib/venv}"

. ${VENVDIR}/${PYVENV}/bin/activate
${VENVDIR}/${PYVENV}/bin/${PYVENV} $@
